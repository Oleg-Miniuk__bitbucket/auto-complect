/* eslint-disable react/jsx-props-no-spreading */
import * as React from "react";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import { connect } from "react-redux";
import { ru } from "date-fns/locale";
import { dispatch } from "../../store/store";

const css = `
.css-o9k5xi-MuiInputBase-root-MuiOutlinedInput-root {
  height: 30px;
}
  `;

const mapStateToProps = (state) => {
  return {
    rawDate: state.deliveryTime.rawDate,
  };
};

function DateSelector(props) {
  const [value, setValue] = React.useState(props.rawDate || new Date());
  const monthsLocalised = {
    Jan: "Январь",
    Feb: "Февраль",
    Mar: "Март",
    Apr: "Апрель",
    May: "Май",
    Jun: "Июнь",
    Jul: "Июль",
    Aug: "Август",
    Sep: "Сентябрь",
    Oct: "Октябрь",
    Nov: "Ноябрь",
    Dec: "Декабрь",
  };
  function handleDateChange(newValue) {
    const dateArray = newValue.toString().split(" ");
    setValue(newValue);
    dispatch({
      type: "deliveryTime/set",
      year: dateArray[3],
      month: dateArray[1],
      monthLocalised: monthsLocalised[dateArray[1]],
      rawDate: newValue,
    });
  }
  return (
    <LocalizationProvider locale={ru} dateAdapter={AdapterDateFns}>
      <style type="text/css">{css}</style>
      <DatePicker
        views={["year", "month"]}
        minDate={new Date("2012-03-01")}
        maxDate={new Date("2099-06-01")}
        value={value}
        onChange={(newValue) => {
          handleDateChange(newValue);
        }}
        renderInput={(params) => <TextField {...params} helperText={null} />}
      />
    </LocalizationProvider>
  );
}

export default connect(mapStateToProps)(DateSelector);
