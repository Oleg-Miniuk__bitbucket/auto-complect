import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import { connect } from "react-redux";

const css = `
@import url("https://fonts.cdnfonts.com/css/ford-antenna");
  .table-container-details {
    background: white;
    border-color: white;
    width: 600px;
  }
  .names-table-row {
    display: flex;
    flex-wrap: nowrap;
    width: 600px;
    flex-direction: row;
    justify-content: space-between;
  }
  .css-rorn0c-MuiTableContainer-root {
    overflow-x: hidden;
  }
  .css-ahj2mt-MuiTypography-root {
   margin: 0px; 
   overflow: hidden;
  }
  .names-table-cell{
    text-align:start;
    width: 120px;
    height: 30px;
    display: table-cell;
    border-bottom: 1px solid rgba(255,255,255);
    padding: 3px 0px 3px 0px;
    margin: 0px 10px 0px 0px;
    font-family: "Roboto";
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    
  }
  .data-table-cell {
    text-align:start;
    width: 120px;
    height: 30px;
    display: table-cell;
    border-bottom: 1px solid rgba(255,255,255);
    padding: 3px 0px 0px 0px;
    margin: 0px 10px 0px 0px;
    font-family: "Roboto";
    font-style: normal;
    font-weight: 700;
    font-size: 14px;
    line-height: 19px;
    color: #333333;
    rgba(51, 51, 51, 0.12);
}
.css-9thqaf-MuiCardContent-root:last-child {
  padding-bottom: 2px;
}
`;

const mapStateToProps = (state) => {
  return {
    engine: state.hullTypes.engine,
    fueltype: state.hullTypes.fuel_type,
    transmission: state.hullTypes.transmission,
    colour: state.colorSetter.currentColorRus,
    salon: state.hullTypes.interior,
    typeofdrive: state.hullTypes.drive_type,
    enginepower: state.hullTypes.engine_power,
    gearbox: state.hullTypes.transmission,
    deliverytime: state.hullTypes.deliveryTime,
  };
};

function DetailsForFinalRequest(props) {
  function createData1(engine, fueltype, transmission, colour) {
    return {
      engine,
      fueltype,
      transmission,
      colour,
    };
  }
  function createData2(salon, typeofdrive, enginepower, gearbox) {
    return {
      salon,
      typeofdrive,
      enginepower,
      gearbox,
    };
  }

  const rows1 = [
    createData1(props.engine, props.fueltype, props.transmission, props.colour),
  ];
  const rows2 = [
    createData2(
      props.salon,
      props.typeofdrive,
      props.enginepower,
      props.gearbox
    ),
  ];
  return (
    <div>
      <style type="text/css">{css}</style>
      <TableContainer className="table-container-details">
        <Table sx={{ p: 4, minWidth: "100%" }}>
          <TableHead>
            <Table className="names-table-row">
              <TableCell className="names-table-cell" sx={{ Width: 30 }}>
                Двигатель
              </TableCell>
              <TableCell className="names-table-cell">Тип топлива</TableCell>
              <TableCell className="names-table-cell">Трансмиссия</TableCell>
              <TableCell className="names-table-cell">Цвет</TableCell>
            </Table>
          </TableHead>
          <TableBody>
            {rows1.map((row1) => (
              <Table className="names-table-row" key={row1.engine}>
                <TableCell
                  className="data-table-cell"
                  component="th"
                  scope="row"
                >
                  {row1.engine}
                </TableCell>
                <TableCell className="data-table-cell">
                  {row1.fueltype}
                </TableCell>
                <TableCell className="data-table-cell">
                  {row1.transmission}
                </TableCell>
                <TableCell className="data-table-cell">{row1.colour}</TableCell>
              </Table>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TableContainer className="table-container-details">
        <Table>
          <TableHead>
            <Table className="names-table-row">
              <TableCell className="names-table-cell">Cалон</TableCell>
              <TableCell className="names-table-cell">Тип привода</TableCell>
              <TableCell className="names-table-cell" sx={{ Width: 30 }}>
                Мощность двигателя
              </TableCell>
              <TableCell className="names-table-cell">
                Коробка передач
              </TableCell>
            </Table>
          </TableHead>
          <TableBody>
            {rows2.map((row2) => (
              <Table
                className="names-table-row"
                key={row2.salon}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell
                  className="data-table-cell"
                  component="th"
                  scope="row"
                >
                  {row2.salon}
                </TableCell>
                <TableCell className="data-table-cell">
                  {row2.typeofdrive}
                </TableCell>
                <TableCell className="data-table-cell">
                  {row2.enginepower}
                </TableCell>
                <TableCell className="data-table-cell">
                  {row2.gearbox}
                </TableCell>
              </Table>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default connect(mapStateToProps)(DetailsForFinalRequest);
