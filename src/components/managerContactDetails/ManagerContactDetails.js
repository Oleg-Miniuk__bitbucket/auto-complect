import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import TextField from "@mui/material/TextField";
import { useDispatch, useSelector } from "react-redux";
import managerContactDetails from "./baseManagers.json";

/* Компонент Box служит в качестве компонента-оболочки. */

/* FormControl Предоставляет контекст, такой как заполненный
    сфокусированный, ошибка,необходимый для ввода формы В FormControl можно
    использовать только одну InputBase */

const css = `
// .css-rezjit {
//   margin-left: 20px;
//   margin-top: 0px;
//   width: 100%;
//   max-width: 455px;
//   height: 377px;
// }
.manager-contact-details {
  margin-left: 20px;
  margin-top: 0px;
  width: 100%;
  max-width: 455px;
  height: 377px;
}`;

export default function ManagerContactDetails() {
  const [name, setName] = React.useState("");
  const dispatch = useDispatch();
  // функция которая считывает выбранное значение

  const handleChange = (event) => {
    setName(event.target.value);
    const newArray = [];
    // не обновляет всю страницу при изменении значения в выпадающем списке
    event.preventDefault();

    managerContactDetails.map((element) => {
      if (element.id === event.target.value) {
        newArray.push(
          element.id,
          element.name,
          element.telephone,
          element.email,
          element.address
        );
        console.log(newArray);
      }
      return newArray;
    });

    const [id, managerNameCurrent, phone, email, address] = newArray;
    dispatch({
      type: "managerCurrent",
      id,
      managerName: managerNameCurrent,
      valueAddress: address,
      valueEmail: email,
      valuePhone: phone,
    });
  };
  // getting current state for textfield

  const address = useSelector(
    (state) => state.menagerContactDetailsReducer.valueAddress
  );
  const email = useSelector(
    (state) => state.menagerContactDetailsReducer.valueEmail
  );
  const phone = useSelector(
    (state) => state.menagerContactDetailsReducer.valuePhone
  );
  return (
    <Box className="manager-contact-details">
      <style type="text/css">{css}</style>
      <Typography
        variant="h6"
        gutterBottom
        component="div"
        style={{ textAlign: "left" }}
      >
        Контактные данные менеджера
      </Typography>
      <Divider orientation="horizontal" style={{ width: "100%" }} />
      <FormControl fullWidth margin="dense">
        <InputLabel id="select-label-manager">Выберите менеджера</InputLabel>
        <Select
          style={{ textAlign: "left" }}
          labelId="select-label-manager"
          id="select-manager"
          label="Выберите менеджера"
          value={name}
          onChange={handleChange}
        >
          {managerContactDetails.map((element) => {
            return <MenuItem value={element.id}>{element.name}</MenuItem>;
          })}
        </Select>
      </FormControl>
      <div>
        <TextField
          fullWidth
          margin="normal"
          value={address}
          id="standard-read-only-input"
          label="Адрес"
          InputProps={{
            readOnly: true,
          }}
          variant="standard"
        />
        <TextField
          fullWidth
          value={phone}
          margin="normal"
          id="standard-read-only-input"
          label="Телефон"
          InputProps={{
            readOnly: true,
          }}
          variant="standard"
        />
        <TextField
          fullWidth
          value={email}
          margin="normal"
          id="standard-read-only-input"
          label="E-mail"
          InputProps={{
            readOnly: true,
          }}
          variant="standard"
        />
      </div>
    </Box>
  );
}
