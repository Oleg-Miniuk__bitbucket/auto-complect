import React from "react";
import { connect } from "react-redux";
import "./basic_options.css";

const mapStateToProps = (state) => {
  return {
    base_options: state.hullTypes.base_options,
  };
};

function BasicOptions(props) {
  return (
    <div className="basic_options_wrapper">
      <div className="basic_options_note">
        <img src="/img/Vector.jpg" className="info_icon" alt="" />
        <p>Данное оборудование является стандартным в вашей конфигурации</p>
      </div>
      {props.base_options.map((baseOption) => (
        <div className="basic_option">
          <div className="basic_option_content">
            <p>{baseOption}</p>
          </div>
          <div className="basic_option_separator" />
        </div>
      ))}
    </div>
  );
}

export default connect(mapStateToProps)(BasicOptions);
