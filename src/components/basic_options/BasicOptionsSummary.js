import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { connect } from "react-redux";

const css = `
@import url("https://fonts.cdnfonts.com/css/ford-antenna");
.css-19midj6 {
    padding: 16px 0px 0px 0px;
}
.css-dsuxgy-MuiTableCell-root {
    font-family: "Ford Antenna",sans-serif;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    padding: 12px 0px;
    color: #333333;
}
.css-ahj2mt-MuiTypography-root {
  font-family: "Ford Antenna",sans-serif;
  overflow: hidden;
}
.css-13xy2my {
  box-shadow: rgb(0 0 0 / 0%) 0px 0px 0px 0px, rgb(0 0 0 / 0%) 0px 0px 0px 0px, rgb(0 0 0 / 0%) 0px 0px 0px 0px;
  overflow-x: hidden;
}
.css-11xur9t-MuiPaper-root-MuiTableContainer-root {
  box-shadow: 0px 0px 0px 0px rgb(0 0 0 / 0%), 0px 0px 0px 0px rgb(0 0 0 / 0%), 0px 0px 0px 0px rgb(0 0 0 / 0%);
}`;

const mapStateToProps = (state) => {
  return {
    reequipBasicOptions: state.reequipToggle.reequipBasicOptions,
    basicOptions: state.hullTypes.base_options,
  };
};

function BasicOptionsSummary(props) {
  const rows = props.basicOptions.concat(props.reequipBasicOptions);
  return (
    <div>
      <style type="text/css">{css}</style>
      <TableContainer component={Paper}>
        <div className="basic_options_note">
          <img src="/img/Vector.jpg" className="info_icon" alt="img" />
          <p>Данное оборудование является стандартным в вашей конфигурации</p>
        </div>
        <Table sx={{ maxWidth: 1128 }} size="small" aria-label="a dense table">
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.index}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row}
                </TableCell>
                <TableCell align="right">{}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default connect(mapStateToProps)(BasicOptionsSummary);
