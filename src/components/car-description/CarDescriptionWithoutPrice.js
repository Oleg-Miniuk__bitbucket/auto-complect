import React from "react";
import { connect } from "react-redux";

const css = `
.box_car_description{
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
}
.hull_name {
      font-family: "Ford Antenna";
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 22px;
      margin-top: 40px;
      color: #2d96cd;
}
.hull_type {
      font-family: "Ford Antenna";
      font-style: normal;
      font-weight: 300px;
      font-size: 14px;
      line-height: 17px;
      margin-top: 4px;
      letter-spacing: 0.1px;
      color: #2d96cd;
}
`;

const mapStateToProps = (state) => {
  return {
    name: state.hullTypes.name,
  };
};

function CarDescriptionWithoutPrice(props) {
  return (
    <div className="box_car_description">
      <style type="text/css">{css}</style>
      <div className="hull_desc">
        <p className="hull_name">{props.name}</p>
        <p className="hull_type">Цельнометаллический фургон</p>
      </div>
    </div>
  );
}

export default connect(mapStateToProps)(CarDescriptionWithoutPrice);
