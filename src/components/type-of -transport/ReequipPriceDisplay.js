/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from "react";
import { connect } from "react-redux";

// компонент для отображения цен в российских и белорусских рублях
// пока что подразумевается отображение базовой цены из "БД" с конвертацией в российские рубли

const mapStateToProps = (state) => {
  return {
    reequipPriceBus: state.toggleBtnBus.reequipPrice,
    reequipPriceCurrencyBus: state.toggleBtnBus.reequipPriceCurrency,
    reequipPriceCargo: state.toggleBtnCargo.reequipPrice,
    reequipPriceCurrencyCargo: state.toggleBtnCargo.reequipPriceCurrency,
    reequipPriceLight: state.toggleBtnLight.reequipPrice,
    reequipPriceCurrencyLight: state.toggleBtnLight.reequipPriceCurrency,
  };
};

function ReequipPriceDisplay(props) {
  let reequipPrice = "";
  let reequipPriceCurrency = "";
  if (props.name === "light") {
    reequipPrice = props.reequipPriceLight || "";
    reequipPriceCurrency = props.reequipPriceCurrencyLight || "";
  }
  if (props.name === "bus") {
    reequipPrice = props.reequipPriceBus || "";
    reequipPriceCurrency = props.reequipPriceCurrencyBus || "";
  }

  if (props.name === "cargo") {
    reequipPrice = props.reequipPriceCargo || "";
    reequipPriceCurrency = props.reequipPriceCurrencyCargo || "";
  }
  // конвертация из числа в строку с пробелом каждые 3 символа для отображения на фронте
  // курсы все временные и переменная base_price пока что предполагает, что базовая цена указана в белорусских рублях
  return (
    <div>
      <p className="hull_spec_prices_byn">
        Цена : {reequipPrice} {}
        {reequipPriceCurrency}
      </p>
    </div>
  );
}

export default connect(mapStateToProps)(ReequipPriceDisplay);
