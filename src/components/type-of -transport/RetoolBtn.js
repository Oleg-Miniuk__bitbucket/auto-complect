import React from "react";
import { Button, Stack } from "@mui/material";
import { useHistory } from "react-router-dom";
import { connect, useDispatch } from "react-redux";

const mapStateToProps = (state) => {
  return {
    reequipToggleId: state.reequipToggle.currentId,
    selectedSchema: state.reequipToggle.selectedSchema,
    rubExRate: state.rubExRateTracker.rubExRate,
    usdExRate: state.usdExRateTracker.usdExRate,
  };
};

function RetoolBtn(props) {
  let currentButtonState = true;
  if (
    props.currentCardId === props.reequipToggleId &&
    props.currentHull === props.selectedSchema
  ) {
    currentButtonState = false;
  }
  const history = useHistory();
  const dispatch = useDispatch();

  function handleClick() {
    history.push({
      pathname: "/configurator_p2",
    });
    dispatch({
      type: "nameCar",
      nameCarRetoolBtn: props,
    });
  }
  return (
    <Stack direction="row">
      <Button
        variant="outlined"
        size="medium"
        fullWidth="true"
        onClick={() => handleClick()}
        disabled={currentButtonState}
      >
        Переоборудовать
      </Button>
    </Stack>
  );
}

export default connect(mapStateToProps)(RetoolBtn);
