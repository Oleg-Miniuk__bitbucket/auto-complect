import * as React from "react";
import { Grid } from "@mui/material";
import { connect } from "react-redux";
import CardDisplay from "./CardDisplay";

const mapStateToProps = (state) => {
  return {
    hullTypes: state.hullTypes,
    reequipmentOptions: state.hullTypes.reequipmentOptions,
  };
};

function СardLayoutTypeTransport(props) {
  console.log(props);
  return (
    <Grid sx={{ flexGrow: 1 }} container spacing={0.5}>
      {props.reequipmentOptions.map((reequipmentOption) => {
        console.log(reequipmentOption);
        return (
          <Grid item xs={4}>
            <Grid container justifyContent="center">
              <Grid>
                <CardDisplay
                  currentCardId={reequipmentOption.id}
                  reequipmentOptions={reequipmentOption}
                  hullTypes={props.hullTypes}
                />
              </Grid>
            </Grid>
          </Grid>
        );
      })}
    </Grid>
  );
}

export default connect(mapStateToProps)(СardLayoutTypeTransport);
