/* eslint-disable no-unused-vars */
import * as React from "react";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import { styled } from "@mui/material/styles";
import { connect, useDispatch, useSelector } from "react-redux";
import { CardMedia } from "@mui/material";
import { dispatch, getState } from "../../store/store";

const mapStateToProps = (state) => {
  return {
    rubExRate: state.rubExRateTracker.rubExRate,
    usdExRate: state.usdExRateTracker.usdExRate,
    selectedSchema: state.reequipToggle.selectedSchema,
  };
};

function SchemaToggleButton(props) {
  const cargoSits = useSelector(
    (state) => state.toggleBtnCargo.selectedSitsNum
  );

  const buttonWidth = Math.min(
    300 / props.reequipment.schemaOptions.length,
    100
  );
  const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
    "& .MuiToggleButtonGroup-grouped": {
      margin: theme.spacing(0.9),
      width: buttonWidth,
      height: 33,
    },
  }));
  // function clearState() {
  //   return dispatch({
  //     type: "clear_STATE",
  //   });
  // }

  function toggleButtonHandler(toggleButtonValue, schemaOption) {
    // clearState();

    dispatch({
      type: `${toggleButtonValue.name}Reequip/set`,
      selectedSitsNum: `${schemaOption.sitsNum}`,
      schemaPicture: `${schemaOption.pic}`,
      reequipBasicOptions: schemaOption.reequipBasicOptions,
      reequipOptions: schemaOption.reequipOptions,
      reequipPrice: schemaOption.reequipPrice,
      reequipPriceCurrency: schemaOption.reequipPriceCurrency,
    });
    dispatch({
      type: `reequipToggle/set`,
      id: toggleButtonValue.id,
      selectedSitsNum: `${schemaOption.sitsNum}`,
      selectedSchema: `${toggleButtonValue.name}`,
      schemaPicture: `${schemaOption.pic}`,
      reequipBasicOptions: schemaOption.reequipBasicOptions,
      reequipOptions: schemaOption.reequipOptions,
      reequipPrice: schemaOption.reequipPrice,
      reequipPriceCurrency: schemaOption.reequipPriceCurrency,
    });
    dispatch({
      type: "reequipHullPrice/set",
      priceByn: Math.round(schemaOption.reequipPrice * props.usdExRate),
      priceUsd: schemaOption.reequipPrice,
      priceRub: Math.round(
        schemaOption.reequipPrice * props.usdExRate * props.rubExRate
      ),
    });
  }
  console.log(props.reequipment.schemaOptions[0].pic);
  let currentImage = "";
  const defaultImage = props.reequipment.schemaOptions[0].pic;
  // let currentImage = props.reequipment.schemaOptions.at[0].pic;
  let alignment = "";

  if (props.reequipment.name === getState().reequipToggle.name) {
    alignment = getState().reequipToggle.selectedSitsNum;
  } else {
    alignment = "";
  }

  const cardSelector = () => {
    if (props.reequipment.name === "cargo") {
      alignment = getState().toggleBtnCargo.selectedSitsNum;
      currentImage = getState().toggleBtnCargo.schemaPicture || defaultImage;
    }
    if (props.reequipment.name === "bus") {
      alignment = getState().toggleBtnBus.selectedSitsNum;
      currentImage = getState().toggleBtnBus.schemaPicture || defaultImage;
    }
    if (props.reequipment.name === "light") {
      alignment = getState().toggleBtnLight.selectedSitsNum;
      currentImage = getState().toggleBtnLight.schemaPicture || defaultImage;
    }
    if (props.selectedSchema !== props.reequipment.name) {
      alignment = "";
    }
    return [alignment, currentImage];
  };
  const [newAlignment, newCurrentImage] = cardSelector();

  return (
    <div>
      <StyledToggleButtonGroup
        color="primary"
        value={alignment}
        onChange={cardSelector}
        exclusive
      >
        {props.reequipSchemas.map((schemaOption) => {
          return (
            <ToggleButton
              value={schemaOption.sitsNum}
              onClick={() =>
                toggleButtonHandler(
                  props.reequipment,
                  schemaOption,
                  props.currentCardId
                )
              }
            >
              {schemaOption.sitsNum}
            </ToggleButton>
          );
        })}
        ;
      </StyledToggleButtonGroup>
      <CardMedia
        height="117px"
        weight="310px"
        component="img"
        alt="Выберете схему комплектации"
        src={newCurrentImage}
      />
    </div>
  );
}

export default connect(mapStateToProps)(SchemaToggleButton);
