import * as React from "react";
import { Card, CardContent, Typography, Box } from "@mui/material";
import { connect } from "react-redux";
// import TotalCarValue from "../total-car-value/Total-car-value";
import RetoolBtn from "./RetoolBtn";
import SchemaToggleButton from "./SchemaToggleButton";
import ReequipPriceDisplay from "./ReequipPriceDisplay";

const mapStateToProps = (state) => {
  return {
    toggleBtnBus: state.toggleBtnBus,
    toggleBtnCargo: state.toggleBtnCargo,
    toggleBtnLight: state.toggleBtnLight,
  };
};
function CardDisplay(props) {
  let configurationScheme;
  if (props.reequipmentOptions.name === "light") {
    configurationScheme = props.toggleBtnLight.selectedSitsNum;
  }
  if (props.reequipmentOptions.name === "bus") {
    configurationScheme = props.toggleBtnBus.selectedSitsNum;
  }

  if (props.reequipmentOptions.name === "cargo") {
    configurationScheme = props.toggleBtnCargo.selectedSitsNum;
  }
  return (
    <Card sx={{ maxWidth: 360 }}>
      <CardContent align="left">
        <Typography gutterBottom variant="h5" component="div">
          {props.reequipmentOptions.displayName}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {`${props.reequipmentOptions.displayName} автомобиль ${configurationScheme} на базе ${props.hullTypes.name}`}
        </Typography>
        <Typography variant="body2" color="InfoText">
          Выбор схемы комплектации:
        </Typography>
        <SchemaToggleButton
          currentCardId={props.currentCardId}
          reequipment={props.reequipmentOptions}
          reequipName={props.reequipmentOptions.name}
          reequipSchemas={props.reequipmentOptions.schemaOptions}
        />
        <Box>
          <ReequipPriceDisplay name={props.reequipmentOptions.name} />
          <RetoolBtn
            currentCardId={props.currentCardId}
            currentHull={props.reequipmentOptions.name}
          />
        </Box>
      </CardContent>
    </Card>
  );
}

export default connect(mapStateToProps)(CardDisplay);
