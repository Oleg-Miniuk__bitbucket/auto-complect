/* eslint-disable camelcase */

import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { StyleSheet } from "@react-pdf/renderer";
import { connect } from "react-redux";
import "./Tab_Optional_Equipment.css";
import { dispatch } from "../../store/store";

const css = `
.MuiDataGrid-root .MuiDataGrid-columnHeader:not(.MuiDataGrid-columnHeader--sorted) .MuiDataGrid-sortIcon {
  opacity: 0;
  font-size: 0;
  transition: opacity 0ms cubic-bezier(0, 0, 0, 0) 0ms;
}
.MuiDataGrid-root .MuiDataGrid-columnHeader, .MuiDataGrid-root .MuiDataGrid-cell {
  padding: 0px;
}
.css-19midj6 {
  padding: 16px 0px 0px 0px;
  overflow: hidden;
}
.MuiDataGrid-root .MuiDataGrid-cell {
  display: block;
  overflow: hidden;
  white-space: normal !important;
  line-height: inherit !important; 
}
.MuiDataGrid-root .MuiDataGrid-window {
  overflow-x: hidden;
}
.MuiDataGrid-root .MuiDataGrid-columnHeaderTitleContainer {
  padding: 0px;
}
.css-ptiqhd-MuiSvgIcon-root {
  font-size: 0rem;
}
`;
const mapStateToProps = (state) => {
  return {
    reequipOptions: state.reequipToggle.reequipOptions,
    selectedPositionsInStore: state.reequipSOption.selectPositions,
    usdExRate: state.usdExRateTracker.usdExRate,
    rubExRate: state.rubExRateTracker.rubExRate,
  };
};

function DataTable(props) {
  const styles = StyleSheet.create({
    gridcolum: {
      width: "100%",
      height: "100%",
    },
  });

  const columns = [
    // { field: "id", headerName: "ID", width: 20 },
    { field: "content", headerName: "Позиция", width: 970 },
    { field: "reequipOptionPrice", headerName: "Цена", width: 65 },
    {
      field: "priceCurrency",
      headerName: " ",
      width: 60,
    },
  ];

  const { selectedPositionsInStore, usdExRate, rubExRate } = props;
  const selectedPositionsIds = selectedPositionsInStore
    ? selectedPositionsInStore.map((selectedPositions) => selectedPositions.id)
    : [];
  console.log(selectedPositionsIds);

  function handleClick(selectedRows) {
    const selectedPositions = props.reequipOptions.filter((hull_option) =>
      selectedRows.find((selectedRow) => selectedRow === hull_option.id)
    );
    const selectedOptionsCost = selectedPositions
      ? selectedPositions
          .map((selectedPosition) => selectedPosition.reequipOptionPrice)
          .reduce((previousPrice, nextPrice) => previousPrice + nextPrice, 0) ||
        0
      : 0;

    dispatch({
      type: "reequipSOption/set",
      selectPositions: selectedPositions,
    });
    dispatch({
      type: "reequipHullOptionsPrice/set",
      priceByn: Math.round(selectedOptionsCost * usdExRate),
      priceRub: Math.round((selectedOptionsCost * usdExRate) / rubExRate),
      priceUsd: selectedOptionsCost,
    });
    dispatch({
      type: "totalPrice/update",
    });
  }
  const rows = props.reequipOptions;

  return (
    <div className="BoxTab_Optional_EquipmenT">
      <style type="text/css">{css}</style>
      <div
        style={{
          height: "100%",
          width: "100%",
          overflow: "auto",
        }}
      >
        <DataGrid
          rowHeight={66}
          style={styles.gridcolum}
          rows={rows}
          columns={columns}
          pageSize={100}
          rowsPerPageOptions={[100]}
          checkboxSelection
          selectionModel={selectedPositionsIds}
          onSelectionModelChange={(newSelect) => {
            handleClick(newSelect);
          }}
        />
      </div>
    </div>
  );
}
export default connect(mapStateToProps)(DataTable);
