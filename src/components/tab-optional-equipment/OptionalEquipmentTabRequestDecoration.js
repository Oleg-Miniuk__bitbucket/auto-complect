import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { connect } from "react-redux";
import "./Tab_Optional_Equipment.css";

const css = `
.MuiDataGrid-root .MuiDataGrid-cell {
  display: block;
  overflow: auto;
  white-space: normal !important;
  line-height: inherit !important; 
}
`;

const columns = [
  // { field: "id", headerName: "ID", width: 20 },
  { field: "option_name", headerName: "Позиция", width: 890 },
  { field: "option_cost", headerName: "Цена", width: 140 },
  {
    field: "option_cost_currency",
    headerName: " ",
    width: 90,
  },
];

const mapStateToProps = (state) => {
  return {
    selectedOptionsRows: state.additionalOptions.selectOptions,
  };
};

function DataTable(props) {
  const { selectedOptionsRows } = props;
  const rows = selectedOptionsRows || [];
  return (
    <div className="BoxTab_Optional_Equipmen">
      <style type="text/css">{css}</style>
      <div style={{ height: 400, width: "100%" }}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
        />
      </div>
    </div>
  );
}
export default connect(mapStateToProps)(DataTable);
