import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { connect } from "react-redux";
import "./Tab_Optional_Equipment.css";

const css = `
.MuiDataGrid-root .MuiDataGrid-cell {
  display: block;
  overflow: auto;
  white-space: normal !important;
  line-height: inherit !important; 
}
`;

const columns = [
  // { field: "id", headerName: "ID", width: 10 },
  { field: "content", headerName: "Позиция", width: 600 },
  { field: "reequipOptionPrice", headerName: "Цена", width: 60 },
  {
    field: "priceCurrency",
    headerName: " ",
    width: 60,
  },
];

const mapStateToProps = (state) => {
  return {
    selectedOptionsRows: state.reequipSOption.selectPositions,
  };
};

function DataTable(props) {
  const { selectedOptionsRows } = props;
  const rows = selectedOptionsRows || [];
  return (
    <div className="BoxTab_Optional_EquipmenTs">
      <style type="text/css">{css}</style>
      <div
        style={{
          height: 560,
          width: "66%",
          marginRight: 0,
          marginLeft: "auto",
        }}
      >
        <DataGrid
          rowHeight={90}
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
        />
      </div>
    </div>
  );
}
export default connect(mapStateToProps)(DataTable);
