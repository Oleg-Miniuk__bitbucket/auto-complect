/* eslint-disable camelcase */
import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import "./Tab_Optional_Equipment.css";
import { StyleSheet } from "@react-pdf/renderer";
import { connect } from "react-redux";
import { dispatch } from "../../store/store";

const css = `
.MuiDataGrid-root .MuiDataGrid-columnHeader:not(.MuiDataGrid-columnHeader--sorted) .MuiDataGrid-sortIcon {
  opacity: 0;
  font-size: 0;
  transition: opacity 0ms cubic-bezier(0, 0, 0, 0) 0ms;
}
.MuiDataGrid-root .MuiDataGrid-cell {
  display: block;
  overflow: auto;
  white-space: normal !important;
  line-height: inherit !important; 
}
.MuiDataGrid-root .MuiDataGrid-columnHeaderTitleContainer {
  padding: 0px;
}
.MuiDataGrid-root .MuiDataGrid-columnHeader, .MuiDataGrid-root .MuiDataGrid-cell {
  padding: 0px; 
}
.css-ptiqhd-MuiSvgIcon-root {
  font-size: 0rem;
}
`;

const mapStateToProps = (state) => {
  return {
    additional_options: state.hullTypes.additional_options,
    selectedOptionsInStore: state.additionalOptions.selectOptions,
    rubExRate: state.rubExRateTracker.rubExRate,
  };
};

function DataTable(props) {
  const { rubExRate } = props;
  const styles = StyleSheet.create({
    gridcolum: {
      width: "100%",
      height: "100%",
    },
  });
  const columns = [
    // { field: "id", headerName: "ID", width: 20 },
    {
      field: "option_name",
      headerName: "Позиция",
      width: 970,
    },
    { field: "option_cost", headerName: "Цена", width: 70 },
    {
      field: "option_cost_currency",
      headerName: " ",
      width: 60,
    },
  ];

  const { selectedOptionsInStore } = props;
  console.log(selectedOptionsInStore);
  const selectedOptionsIds = selectedOptionsInStore
    ? selectedOptionsInStore.map((selectedOption) => selectedOption.id)
    : [];

  function handleClick(selectedRows) {
    const selectedOptions = props.additional_options.filter((hull_option) =>
      selectedRows.find((selectedRow) => selectedRow === hull_option.id)
    );
    const selectedOptionsCost = selectedOptions
      ? selectedOptions
          .map((selectedOption) => selectedOption.option_cost)
          .reduce((previousPrice, nextPrice) => previousPrice + nextPrice, 0) ||
        0
      : 0;
    dispatch({
      type: "additionalOptions/set",
      selectOptions: selectedOptions,
    });
    dispatch({
      type: "additionalOptionsPrice/set",
      priceByn: Math.round(selectedOptionsCost / rubExRate),
      priceRub: selectedOptionsCost,
    });
    dispatch({
      type: "totalPrice/update",
    });
  }

  const rows = props.additional_options || [];

  return (
    <div className="BoxTab_Optional_Equipment_Co">
      <style type="text/css">{css}</style>
      <div
        style={{
          height: "100%",
          width: "100%",
          overflow: "auto",
        }}
      >
        <div className="test">
          <img
            alt="info_iconTab_Optional_Equipment"
            src="/img/Vector.jpg"
            className="info_iconTab_Optional_Equipment"
          />
          <div className="Tab_Optional_Equipment_com">
            При добавлении опционального оборудования, цена автомобиля изменится
          </div>
        </div>
        {/* rowHeight={26} */}
        <DataGrid
          autoHeight="true"
          style={styles.gridcolum}
          rows={rows}
          columns={columns}
          pageSize={100}
          rowsPerPageOptions={[100]}
          checkboxSelection
          selectionModel={selectedOptionsIds}
          onSelectionModelChange={(newSelect) => {
            handleClick(newSelect);
          }}
        />
      </div>
    </div>
  );
}

export default connect(mapStateToProps)(DataTable);
