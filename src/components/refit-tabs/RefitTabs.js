import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import TotalCarValue from "../total-car-value/Total-car-value";
import GotoCommercialProposalBtn from "../generic/GotoCommercialProposal_btn";
import BasicOptionsReEquipment from "../basic_options/BasicOptionsReEquipment";
import OptionalEquipmentTabRequipment from "../tab-optional-equipment/OptionalEquipmentTabRequipment";

const css = `
@import url("https://fonts.cdnfonts.com/css/ford-antenna");
  .box {
    background: white;
    width: 1128px;
    position: "relative";
    min-height: 200px;
    margin: 20px 0px 0px 0px;
    border-color: transparent;
  }
  .appbar{
    background: white;
      color: white;
  }
  .MuiTabs-indicator{
    color: #2D96CD; 
  }
  .tab1{
    font-family: "Ford Antenna";
    color: #717171;
    padding: 0px;
    margin: 12px 200px 12px 0px;
  }
  .tab2{
    font-family: "Ford Antenna";
    color: #717171;
    padding: 0px;
    margin: 12px 200px 12px 0px;
  }
  .tab3{
    font-family: "Ford Antenna";
    color: #717171;
    padding: 0px;
    margin: 12px 0px 12px 0px;
  }
  .swipeableviews{
    overflow: hidden;
  }
  .price_list_btn_plas_next_btn{
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
  }
  .price_list_btn_class{
    margin-right: 0px;
  }
  .css-1h9z7r5-MuiButtonBase-root-MuiTab-root.Mui-selected {
    color: #2D96CD;
  }
  .css-1aquho2-MuiTabs-indicator {
      background-color: #2D96CD;
  }
  .css-hip9hq-MuiPaper-root-MuiAppBar-root {
    box-shadow: 0px 2px 4px -1px rgb(0 0 0 / 0%), 0px 4px 5px 0px rgb(0 0 0 / 0%), 0px 1px 10px 0px rgb(0 0 0 / 0%);
  }
  .css-1h9z7r5-MuiButtonBase-root-MuiTab-root{
      align-items: start;
  } 
  .css-19kzrtu {
    padding: 24px 0px;
    overflow: hidden;
  }
  .css-1gsv261 {
    border-bottom: 1px solid;
    border-color: rgba(0, 0, 0, 0);
  }
`;

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      // other={other}
    >
      {value === index && (
        <Box sx={{ p: 2 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};
TabPanel.defaultProps = {
  children: "",
};

function a11yProps(index) {
  return {
    id: `action-tab-${index}`,
    "aria-controls": `action-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="App">
      <style type="text/css">{css}</style>
      <Box className="box" sx={{ width: "100%" }}>
        <Box
          className="swipeableviews"
          sx={{ borderBottom: 1, borderColor: "divider" }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <Tab
              className="tab1"
              label="Базовая комплектация"
              a11yProps={a11yProps(0)}
            />
            <Tab
              className="tab2"
              label="Опции переоборудования"
              a11yProps={a11yProps(1)}
            />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <BasicOptionsReEquipment />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <OptionalEquipmentTabRequipment />
          <TotalCarValue />
          <GotoCommercialProposalBtn />
        </TabPanel>
      </Box>
    </div>
  );
}
