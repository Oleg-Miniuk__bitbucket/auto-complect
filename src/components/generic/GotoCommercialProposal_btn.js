import * as React from "react";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";

const css = `
@import url("https://fonts.cdnfonts.com/css/ford-antenna");
  .goto_commercial-proposal_btn_wrapper {
   display: flex;
   width: 100%;
   margin-right: 0px;
   margin-left : 0px;
   overflow: hidden;
   justify-content: flex-end;
  }
  .gotoCom {
    display: flex;
    justify-content: center;
    height: 46px;
    width: 300px;
    top: -1.5px;
    border-radius: 0px;
    padding: 0px;
    font-family: Ford Antenna;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 22px;
    letter-spacing: 0.10000000149011612px;
    text-align: left;
    color: #FFFFFF;  
    background-color: #2D96CD;
  }`;

const mapStateToProps = (state) => {
  return {
    currentId: state.reequipToggle.currentId,
  };
};

function GotoCommercialProposalBtn(props) {
  let buttonState = true;
  if (props.currentId !== undefined) {
    buttonState = false;
  }
  const history = useHistory();
  // const dispatch = useDispatch();

  function handleClick() {
    history.push({
      pathname: "/сonfigurator_p3",
    });
    // dispatch({
    //   type: "nameCar",
    //   nameCarRetoolBtn: props,
    // });
  }
  return (
    <div className="">
      <style type="text/css">{css}</style>
      <Stack className="goto_commercial-proposal_btn_wrapper" direction="row">
        <Button
          className="gotoCom"
          fullWidth
          variant="contained"
          onClick={() => handleClick()}
          disabled={buttonState}
        >
          Перейти к оформлению КП
        </Button>
      </Stack>
    </div>
  );
}

export default connect(mapStateToProps)(GotoCommercialProposalBtn);
