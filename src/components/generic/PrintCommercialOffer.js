import * as React from "react";
import { connect } from "react-redux";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useHistory } from "react-router-dom";

const css = `
@import url("https://fonts.cdnfonts.com/css/ford-antenna");
  .print_commercial_offer_stack {
    
  }
  .print_commercial_offer_button{
    height: 46px;
    width: 455px;
    left: 0px;
    top: -1.5px;
    border-radius: 0px;
    padding: 0px;
    font-family: Ford Antenna;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 22px;
    letter-spacing: 0.10000000149011612px;
    text-align: left;
    color: #FFFFFF;  
    background-color: #2D96CD;
  }`;

const mapStateToProps = (state) => {
  return {
    name: state.hullTypes.name,
    year: state.deliveryTime.year,
    monthLocalised: state.deliveryTime.monthLocalised,
    managerName: state.menagerContactDetailsReducer.managerName,
    valuePhone: state.menagerContactDetailsReducer.valuePhone,
    valueEmail: state.menagerContactDetailsReducer.valueEmail,
    source: state.colorSetter.currentCarPic,
    engine: state.hullTypes.engine,
    fueltype: state.hullTypes.fuel_type,
    transmission: state.hullTypes.transmission,
    colour: state.colorSetter.currentColorRus,
    salon: state.hullTypes.interior,
    typeofdrive: state.hullTypes.drive_type,
    enginepower: state.hullTypes.engine_power,
    gearbox: state.hullTypes.transmission,
    deliverytime: state.hullTypes.deliveryTime,
    reequipBasicOptions: state.reequipToggle.reequipBasicOptions,
    basicOptions: state.hullTypes.base_options,
    deliveryTimeYear: state.deliveryTime.year,
    deliveryTimeMonth: state.deliveryTime.monthLocalised,
    optionsName: state.additionalOptions.selectOptions,
    nameCar: state.reequipToggle.selectedSchema,
    reequipToggle: state.reequipToggle.schemaPicture,
    selectedSitsNum: state.reequipToggle.selectedSitsNum,
    selectedSitsNumLight: state.toggleBtnLight.selectedSitsNum,
    selectedSitsNumBus: state.toggleBtnBus.selectedSitsNum,
    selectedSitsNumCargo: state.toggleBtnCargo.selectedSitsNum,
    selectedOptionsRows: state.reequipSOption.selectPositions,
    totalPriceByn: state.priceTracker.totalPriceByn,
    overridenPriceByn: state.priceTracker.overridenPriceByn,
  };
};
function PrintCommercialOffer(props) {
  const history = useHistory();
  function handleClick() {
    // TODO: some logic with store

    history.push({
      pathname: "/pdfPage",
      state: {
        name: props.name,
        year: props.year,
        monthLocalised: props.monthLocalised,
        managerName: props.managerName,
        valuePhone: props.valuePhone,
        valueEmail: props.valueEmail,
        source: props.source,
        engine: props.engine,
        fueltype: props.fueltype,
        transmission: props.transmission,
        colour: props.colour,
        salon: props.salon,
        typeofdrive: props.typeofdrive,
        enginepower: props.enginepower,
        gearbox: props.gearbox,
        deliverytime: props.deliverytime,
        reequipBasicOptions: props.reequipBasicOptions,
        basicOptions: props.basicOptions,
        deliveryTimeYear: props.deliveryTimeYear,
        deliveryTimeMonth: props.deliveryTimeMonth,
        optionsName: props.optionsName,
        nameCar: props.nameCar,
        reequipToggle: props.reequipToggle,
        selectedSitsNum: props.selectedSitsNum,
        selectedSitsNumLight: props.selectedSitsNumLight,
        selectedSitsNumBus: props.selectedSitsNumBus,
        selectedSitsNumCargo: props.selectedSitsNumCargo,
        selectedOptionsRows: props.selectedOptionsRows,
        totalPriceByn: props.totalPriceByn,
        overridenPriceByn: props.overridenPriceByn,
      },
    });
  }
  return (
    <div className="car_cost_including_options_wrapper">
      <style type="text/css">{css}</style>
      <Stack className="print_commercial_offer_stack" direction="row">
        <Button
          className="print_commercial_offer_button"
          variant="contained"
          href=""
          onClick={() => handleClick()}
        >
          Напечатать Коммерческое Предложение
        </Button>
      </Stack>
    </div>
  );
}
export default connect(mapStateToProps)(PrintCommercialOffer);
