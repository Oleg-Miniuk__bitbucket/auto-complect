import * as React from "react";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { dispatch } from "../../store/store";

// компонент для перехода на следующую страницу для оформления КП
// Через пропсы в компонент передаётся объект hull_types, который хранит в себе данные о кузове

const mapStateToProps = (state) => {
  return {
    rubExRate: state.rubExRateTracker.rubExRate,
  };
};

const css = `
@import url("https://fonts.cdnfonts.com/css/ford-antenna");
  .configurator_btn{
    width: 213px;
    height: 43px;
    border-radius: 0px;
    border: 1px solid #2d96cd;
    box-sizing: border-box;
    background-color: #2D96CD ;
    color: white;
    font-family: "Ford Antenna";
    text-transform: unset;
    font-size: 16px;
    line-height: 22px;
  }`;

function ConfiguratorBtn(props) {
  const { rubExRate } = props;
  const history = useHistory();
  function handleClick(hullTypes) {
    history.push({
      pathname: "/configurator_p1",
      state: { hullTypes },
    });
    dispatch({
      type: "hull_types/set",
      name: props.hull_types.name,
      shortName: props.hull_types.shortName,
      engine: props.hull_types.engine,
      fuel_type: props.hull_types.fuel_type,
      transmission: props.hull_types.transmission,
      interior: props.hull_types.interior,
      drive_type: props.hull_types.drive_type,
      engine_power: props.hull_types.engine_power,
      base_price: props.hull_types.base_price,
      base_price_currency: props.hull_types.base_price_currency,
      mass: props.hull_types.mass,
      pictures: props.hull_types.pictures,
      base_options: props.hull_types.base_options,
      additional_options: props.hull_types.additional_options,
      reequipmentOptions: props.hull_types.reequipment,
      reequipOptions: props.hull_types.reequipOptions,
    });
    dispatch({
      type: "priceTracker/init",
      priceByn: Math.round(props.hull_types.base_price / rubExRate),
      priceRub: props.hull_types.base_price,
    });
    dispatch({
      type: "totalPrice/update",
    });
    dispatch({
      type: "color/set",
      id: props.hull_types.pictures[5].id,
      color: props.hull_types.pictures[5].color,
      colorRus: props.hull_types.pictures[5].colorRus,
      source: props.hull_types.pictures[5].source,
    });
  }
  return (
    <Stack direction="row" spacing={2}>
      <style type="text/css">{css}</style>
      <Button
        className="configurator_btn"
        variant="contained"
        onClick={() => handleClick(props.hull_types)}
      >
        Конфигуратор
      </Button>
    </Stack>
  );
}

export default connect(mapStateToProps)(ConfiguratorBtn);
