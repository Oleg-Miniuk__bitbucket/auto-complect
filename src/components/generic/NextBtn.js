/* eslint-disable react/no-unused-state */
/* eslint-disable new-cap */
/* eslint-disable class-methods-use-this */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

export default class NextButton extends Component {
  constructor(props) {
    super(props);
    this.state = { canvas: null };
  }

  printDocument() {
    // const inputEl = useRef(null);
    // const onButtonClick = () => {
    //   // `current` указывает на смонтированный элемент `input`
    //   inputEl.current.focus();
    // };

    const input = document.getElementById("divToPrint");
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF();
      pdf.addImage(imgData, "JPEG", 0, 0);
      pdf.save("download.pdf");
    });
  }

  render() {
    return (
      <div className="nextBtn">
        <button type="button" onClick={this.printDocument}>
          Сохранить
        </button>
      </div>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<NextButton />, rootElement);
// старый код:
// import React from "react";
// import { useHistory } from "react-router-dom";

// // this component for going to next page

// export default function NextButton() {
//   const history = useHistory();

//   function handleClick() {
//     history.push("/сonfigurator_p3");
//   }
//   return (
//     <div className="nextBtn">
//       <button type="button" onClick={() => handleClick()}>
//         Дальше
//       </button>
//     </div>
//   );
// }
