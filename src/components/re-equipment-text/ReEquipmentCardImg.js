import * as React from "react";
import "./imageReEqupment.css";
import { useSelector } from "react-redux";

export default function ReEquipmentCardImg() {
  const nameCar = useSelector(
    (state) => state.retoolBtn.nameCarRetoolBtn.currentHull
  );
  let selectedPic;

  if (nameCar === "light") {
    selectedPic = useSelector((state) => state.toggleBtnLight.schemaPicture);
  }
  if (nameCar === "bus") {
    selectedPic = useSelector((state) => state.toggleBtnBus.schemaPicture);
  }
  if (nameCar === "cargo") {
    selectedPic = useSelector((state) => state.toggleBtnCargo.schemaPicture);
  }

  return (
    <div className="imageReEqupment">
      <img src={selectedPic} alt="схема" width="456px" height="174px" />
    </div>
  );
}
