import * as React from "react";
import { useSelector } from "react-redux";
import { Typography } from "@mui/material";
import "./imageVehicleTypeImgTabs.css";

export default function VehicleTypeImgRequestExecutionTabs() {
  const hullTypesName = useSelector((state) => state.hullTypes.name);
  const reequipToggle = useSelector(
    (state) => state.reequipToggle.schemaPicture
  );
  const selectedSitsNum = useSelector(
    (state) => state.reequipToggle.selectedSitsNum
  );
  const shortName = useSelector((state) => state.hullTypes.shortName);

  const nameCar = useSelector((state) => state.reequipToggle.selectedSchema);
  let fullNameCar;

  if (nameCar === "light") {
    fullNameCar = "Легковой";
  }
  if (nameCar === "bus") {
    fullNameCar = "Автобус";
  }
  if (nameCar === "cargo") {
    fullNameCar = "Грузопассажирский";
  }

  return (
    <div className="typeVehileTab">
      <Typography className="text_h4" variant="h4" paragraph>
        {fullNameCar}
      </Typography>
      <Typography className="text_h1" variant="h6" paragraph>
        {hullTypesName}
      </Typography>
      <Typography className="text_h1" variant="h7" paragraph>
        {`${fullNameCar} ${selectedSitsNum} на базе ${shortName}`}
      </Typography>
      <img src={reequipToggle} alt="схема" width="310px" height="117px" />
    </div>
  );
}
