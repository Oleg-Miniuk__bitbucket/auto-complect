import * as React from "react";
import { connect } from "react-redux";

const css = `
.optional_eq_content_p {
    font-family: "Roboto";
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 17px;
    flex: none;
    order: 0;
    flex-grow: 0;
    margin: 0px 4px;
    margin-bottom: 12px;
    margin-top: 12px;
    text-align: left;
}
`;
const mapStateToProps = (state) => {
  return {
    optionsName: state.additionalOptions.selectOptions,
  };
};

function OptionalEquipmentTabRequestDecorationFinal(props) {
  const { optionsName } = props;
  const rows = optionsName
    ? optionsName.map((optionName) => <li>{optionName.option_name}</li>)
    : [];
  return (
    <div className="optional_eq_container">
      <style type="text/css">{css}</style>
      <div className="optional_eq_content_p">{rows}</div>
    </div>
  );
}

export default connect(mapStateToProps)(
  OptionalEquipmentTabRequestDecorationFinal
);
