/* eslint-disable camelcase */
import React from "react";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    reequipBasicOptions: state.reequipToggle.reequipBasicOptions,
    basicOptions: state.hullTypes.base_options,
  };
};

const css = `
.basic_option_content_p {
    font-family: "Roboto";
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 17px;
    flex: none;
    order: 0;
    flex-grow: 0;
    margin: 0px 4px;
    margin-bottom: 12px;
    margin-top: 12px;
    text-align: left;
}
`;

function BasicOptions(props) {
  const { basicOptions, reequipBasicOptions } = props;
  const rows = basicOptions.concat(reequipBasicOptions);
  const row = rows ? rows.map((baseOption) => <li>{baseOption}</li>) : [];

  return (
    <div className="basic_options_container">
      <style type="text/css">{css}</style>
      <div className="basic_option_content_p">{row}</div>
    </div>
  );
}

export default connect(mapStateToProps)(BasicOptions);
