/* eslint-disable import/no-named-as-default-member */
/* eslint-disable camelcase */
import * as React from "react";
import { connect } from "react-redux";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { StyleSheet } from "@react-pdf/renderer";
import Typography from "@mui/material/Typography";
import BlockFordCards from "../BlockFordCards";
import BasicOptionsFinal from "./BasicOptionsFinal";
import DetailsForFinalRequest from "../details/DetailsForFinalRequest";
import VehicleTypeImgRequestExecutionTabs from "../request-execution-tabs/VehicleTypeImgRequestExecutionTabs";
// import OptionalEquipmentTabVehicleType from "../tab-optional-equipment/OptionalEquipmentTabRequestDecoration";
import OptionalEquipmentTabRequestDecorationFinal from "./OptionalEquipmentTabRequestDecorationFinal";
import store from "../../store/store";

const css = `
.css-w00xr2-MuiPaper-root-MuiCard-root {
  border-radius: 0px;
  box-shadow: 0px 2px 1px -1px rgb(0 0 0 / 0%), 0px 1px 1px 0px rgb(0 0 0 / 0%), 0px 1px 3px 0px rgb(0 0 0 / 0%);
  overflow: hidden;
}
.css-133e15b-MuiPaper-root-MuiCard-root {
    box-shadow: 0px 2px 1px -1px rgb(0 0 0 / 0%), 0px 1px 1px 0px rgb(0 0 0 / 0%), 0px 1px 3px 0px rgb(0 0 0 / 0%); 
    border-radius: 0px;
    overflow: hidden;
}
.css-106781y-MuiTypography-root {
  margin-top: 0px;
  padding: 1px;
  display: table;
}
.rows_li{
  text-align: left;
}
.rows_li_1{
  text-align: left;
  margin-top: 8px;
}
`;

function FinalVehicleInformation(props) {
  const styles = StyleSheet.create({
    hullName: {
      fontFamily: "Roboto",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "16px",
      lineHeight: "22px",
      marginTop: "40px",
      color: "#FFFFFF",
      padding: 1,
    },
    text1: {
      marginTop: 1,
      display: "flex",
      fontSize: "15px",
      textIndent: 20,
      width: "100%",
      fontFamily: "Roboto",
      textAlign: "left",
    },
  });
  const hull_types = store.getState();
  return (
    <div>
      <Card
        sx={{
          display: "flex",
          width: "100%",
          justifyContent: "space-between",
          marginBlock: 0,
          paddingBlock: 0,
        }}
      >
        <style type="text/css">{css}</style>
        <BlockFordCards
          sx={{ width: 600, height: 250 }}
          hull_types={hull_types.hullTypes}
        />
        <Box sx={{ display: "flex", flexDirection: "column" }}>
          <CardContent sx={{ flex: "0 1 auto", padding: 0, width: 600 }}>
            <Box
              sx={{
                backgroundColor: "#2d96cd",
                width: "100%",
                margin: 0,
              }}
            >
              <Typography sx={styles.hullName}>
                {props.name} Цельнометаллический фургон
              </Typography>
            </Box>
            <DetailsForFinalRequest />
            <Card
              sx={{
                display: "flex",
                width: "100%",
                justifyContent: "end",
                marginBlock: 0,
                paddingBlock: 0,
              }}
            >
              <style type="text/css">{css}</style>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <CardContent
                  sx={{
                    flex: "0 1 auto",
                    padding: 0,
                    width: 600,
                  }}
                >
                  <Box
                    sx={{
                      backgroundColor: "#2d96cd",
                      width: "100%",
                      margin: 0,
                    }}
                  >
                    <Typography sx={styles.hullName}>
                      Стоимость по прайс-листу: /* здесь стоимость */
                    </Typography>
                    <Typography sx={styles.hullName}>
                      Специальная цена:
                    </Typography>
                    <Typography sx={styles.hullName}>
                      По курсу НБ РБ на 04.08:
                    </Typography>
                  </Box>
                </CardContent>
              </Box>
            </Card>
          </CardContent>
        </Box>
      </Card>
      <Card
        sx={{
          display: "flex",
          width: "100%",
          justifyContent: "end",
          marginBlock: 0,
          paddingBlock: 0,
        }}
      >
        <style type="text/css">{css}</style>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <CardContent
            sx={{
              flex: "0 1 auto",
              padding: 0,
              width: 600,
            }}
          >
            <Box
              sx={{
                backgroundColor: "#2d96cd",
                width: "100%",
                marginTop: 1,
              }}
            >
              <Typography sx={styles.hullName}>
                Срок поставки: {props.monthLocalised} {props.year}
              </Typography>
            </Box>
          </CardContent>
        </Box>
      </Card>
      <Box
        sx={{
          width: "100%",
          height: "auto",
        }}
      >
        <Box
          sx={{
            backgroundColor: "#2d96cd",
            width: "100%",
            marginTop: 1,
          }}
        >
          <Typography sx={styles.hullName}>Базовая комплектация:</Typography>
        </Box>
        <BasicOptionsFinal />
      </Box>
      <Box
        sx={{
          width: "100%",
          height: "auto",
        }}
      >
        <Box
          sx={{
            backgroundColor: "#2d96cd",
            width: "100%",
            marginTop: 0,
          }}
        >
          <Typography sx={styles.hullName}>
            Опциональное оборудование:
          </Typography>
        </Box>
        <OptionalEquipmentTabRequestDecorationFinal />
      </Box>
      <Box
        sx={{
          width: "100%",
          height: "auto",
        }}
      >
        <Box
          sx={{
            backgroundColor: "#2d96cd",
            width: "100%",
          }}
        >
          <Typography sx={styles.hullName}>
            Тип транспортного срества:
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: 1,
          }}
        >
          <VehicleTypeImgRequestExecutionTabs />

          {/* <OptionalEquipmentTabVehicleType /> */}
        </Box>
      </Box>
      <Box
        sx={{
          backgroundColor: "#2d96cd",
          width: "100%",
          height: "auto",
          marginTop: 39.5,
        }}
      >
        <Typography sx={styles.hullName}>Гарантия на автомобиль:</Typography>
      </Box>
      <li className="rows_li_1">2 года без ограничения пробега</li>
      <li className="rows_li"> 12 лет от сквозной коррозии кузова</li>
      <li className="rows_li"> 2 года на лакокрасочное покрытие</li>
      <li className="rows_li">
        Страна происхождения товара – Российская Федерация.
      </li>
      <Box
        sx={{
          backgroundColor: "#2d96cd",
          width: "100%",
          marginTop: 2,
        }}
      >
        <Typography sx={styles.hullName}>Межсервисный интервал:</Typography>
      </Box>
      <li className="rows_li_1">
        Один раз в год ИЛИ через 20 000 км пробега автомобиля Контактное лицо
      </li>
      <Box
        sx={{
          backgroundColor: "#2d96cd",
          width: "100%",
          marginTop: 2,
        }}
      >
        <Typography sx={styles.hullName}>Контактное лицо:</Typography>
      </Box>
      <li className="rows_li_1">Cпециалист отдела корпаративных продаж:</li>
      <Typography sx={styles.text1}>{props.managerName}</Typography>
      <Typography sx={styles.text1}>{props.valuePhone}</Typography>
      <Typography sx={styles.text1}>{props.valueEmail}</Typography>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    name: state.hullTypes.name,
    year: state.deliveryTime.year,
    monthLocalised: state.deliveryTime.monthLocalised,
    managerName: state.menagerContactDetailsReducer.managerName,
    valuePhone: state.menagerContactDetailsReducer.valuePhone,
    valueEmail: state.menagerContactDetailsReducer.valueEmail,
  };
};
export default connect(mapStateToProps)(FinalVehicleInformation);
