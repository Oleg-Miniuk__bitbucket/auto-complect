/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from "react";
import { connect } from "react-redux";

// компонент для отображения цен в российских и белорусских рублях

const mapStateToProps = (state) => {
  return {
    rubExRate: state.rubExRateTracker.rubExRate,
    basePriceByn: state.priceTracker.basePriceByn,
    basePriceRub: state.priceTracker.basePriceRub,
    additionalOptionsPriceByn: state.priceTracker.additionalOptionsPriceByn,
    additionalOptionsPriceRub: state.priceTracker.additionalOptionsPriceRub,
    reequipHullByn: state.priceTracker.reequipHullByn,
    reequipHullRub: state.priceTracker.reequipHullRub,
    reequipHullOptionsByn: state.priceTracker.reequipHullOptionsByn,
  };
};

function PriceDisplay(props) {
  const {
    rubExRate,
    basePriceByn,
    basePriceRub,
    additionalOptionsPriceByn,
    additionalOptionsPriceRub,
    reequipHullByn,
    reequipHullRub,
    reequipHullOptionsByn,
  } = props;
  const displayedPriceByn =
    basePriceByn +
    additionalOptionsPriceByn +
    reequipHullByn +
    reequipHullOptionsByn;
  const displayedPriceRub =
    basePriceRub +
    additionalOptionsPriceRub +
    reequipHullRub +
    (Math.round(reequipHullOptionsByn * rubExRate) || 0);
  console.log(displayedPriceRub);
  // конвертация из числа в строку с пробелом каждые 3 символа для отображения на фронте
  // курсы все временные и переменная base_price пока что предполагает, что базовая цена указана в белорусских рублях
  // const basePriceRub = displayedPriceByn
  //   .toString()
  //   .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  // const basePriceByn = displayedPriceByn
  //   .toString()
  //   .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  return (
    <div>
      <p className="hull_spec_prices_byn">
        {displayedPriceByn.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} BYN
      </p>
      <p className="hull_spec_prices_rub">
        {displayedPriceRub.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} RUB
      </p>
    </div>
  );
}

export default connect(mapStateToProps)(PriceDisplay);
