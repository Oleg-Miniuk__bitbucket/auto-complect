/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from "react";
import { connect } from "react-redux";

// компонент для отображения цен в российских и белорусских рублях
// пока что подразумевается отображение базовой цены из "БД" с конвертацией в российские рубли

const mapStateToProps = (state) => {
  return {
    rubExRate: state.rubExRateTracker.rubExRate,
  };
};

function PriceDisplayStatic(props) {
  const { rubExRate } = props;
  const displayedPrice = props.base_price || 0;
  // конвертация из числа в строку с пробелом каждые 3 символа для отображения на фронте
  // курсы все временные и переменная base_price пока что предполагает, что базовая цена указана в белорусских рублях
  const base_price = displayedPrice
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  const base_price_byn = Math.round(props.base_price / rubExRate)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  return (
    <div>
      <p className="hull_spec_prices_byn">Цена от {base_price_byn} BYN</p>
      <p className="hull_spec_prices_rub">от {base_price} RUB</p>
    </div>
  );
}

export default connect(mapStateToProps)(PriceDisplayStatic);
