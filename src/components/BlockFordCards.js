import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { connect } from "react-redux";
import { dispatch } from "../store/store";

const css = `
  .ford_pic {
    max-height: 404px;
    max-width: 100%;
    position: relative;
    padding: 0px;
    border-right: 0px; 
  }
  .button_img{
    height: 100%;
    width: 100%;
  }
  .css-10d9dml-MuiTabs-indicator {
    background-color: white;
  }
  
  .css-7ozjwe {
    background-color: white;
}
`;

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      // other={other}
    >
      {value === index && <Box sx={{ p: 3, padding: 0 }}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};
TabPanel.defaultProps = {
  children: "",
};

function a11yprops(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const mapStateToProps = (state) => {
  return {
    id: state.colorSetter.id,
    color: state.colorSetter.color,
    colorRus: state.colorSetter.colorRus,
    source: state.colorSetter.currentCarPic,
  };
};

function BlockFordCards(props) {
  const { id } = props;
  const selectedPictureId = id || 0;
  const [value, setValue] = React.useState(selectedPictureId);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const colorSelect = (picture) => {
    dispatch({
      type: "color/set",
      id: picture.id,
      color: picture.color,
      colorRus: picture.colorRus,
      source: picture.source,
    });
  };

  return (
    <Box sx={props.sx}>
      <style type="text/css">{css}</style>
      <Box sx={{ width: "100%", padding: 0 }}>
        {props.hull_types.pictures.map((picture) => {
          return (
            <TabPanel value={value} index={picture.id}>
              <img src={picture.source} className="ford_pic" alt="" />
            </TabPanel>
          );
        })}
      </Box>
      <Tabs
        className="img_grid"
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 0, width: "22%" }}
      >
        {props.hull_types.pictures.map((picture) => {
          return (
            <Tab
              sx={{ width: "100%" }}
              a11yprops={a11yprops(picture.id)}
              value={value}
              component={React.forwardRef(() => (
                <Button
                  sx={{ width: "100%", padding: 0 }}
                  onClick={() => [setValue(picture.id), colorSelect(picture)]}
                >
                  <img src={picture.source} className="button_img" alt="" />
                </Button>
              ))}
            />
          );
        })}
      </Tabs>
    </Box>
  );
}

export default connect(mapStateToProps)(BlockFordCards);
