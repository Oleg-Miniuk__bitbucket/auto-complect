const defaultState = {
  selectedSitsNum: "",
  schemaPicture: "",
  reequipBasicOptions: [],
  reequipOptions: [],
};

export default function reequipToggle(reequip = defaultState, action) {
  switch (action.type) {
    case "reequipToggle/set":
      return {
        currentId: action.id,
        selectedSitsNum: action.selectedSitsNum,
        selectedSchema: action.selectedSchema,
        schemaPicture: action.schemaPicture,
        reequipBasicOptions: action.reequipBasicOptions,
        reequipOptions: action.reequipOptions,
        reequipPrice: action.reequipPrice,
        reequipPriceCurrency: action.reequipPriceCurrency,
      };
    default:
      return reequip;
  }
}
