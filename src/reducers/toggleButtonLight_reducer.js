const defaultState = {
  selectedSitsNum: " ",
  schemaPicture: "",
  reequipBasicOptions: [],
  reequipOptions: [],
};

export default function toggleBtnLight(stateToggLight = defaultState, action) {
  switch (action.type) {
    case "lightReequip/set":
      return {
        selectedSitsNum: action.selectedSitsNum,
        schemaPicture: action.schemaPicture,
        reequipBasicOptions: action.reequipBasicOptions,
        reequipOptions: action.reequipOptions,
        reequipPrice: action.reequipPrice,
        reequipPriceCurrency: action.reequipPriceCurrency,
      };
    case "clear_STATE":
      return {
        selectedSitsNum: " ",
        schemaPicture: "",
        reequipBasicOptions: [],
        reequipOptions: [],
      };
    default:
      return stateToggLight;
  }
}
