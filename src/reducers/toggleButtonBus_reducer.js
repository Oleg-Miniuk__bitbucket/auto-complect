const defaultState = {
  selectedSitsNum: " ",
  schemaPicture: "",
  reequipBasicOptions: [],
  reequipOptions: [],
};

export default function toggleBtnBus(stateToggBus = defaultState, action) {
  switch (action.type) {
    case "busReequip/set":
      return {
        selectedSitsNum: action.selectedSitsNum,
        schemaPicture: action.schemaPicture,
        reequipBasicOptions: action.reequipBasicOptions,
        reequipOptions: action.reequipOptions,
        reequipPrice: action.reequipPrice,
        reequipPriceCurrency: action.reequipPriceCurrency,
      };
    case "bus_clear_STATE":
      return {
        selectedSitsNum: " ",
        schemaPicture: "",
        reequipBasicOptions: [],
        reequipOptions: [],
      };
    default:
      return stateToggBus;
  }
}
