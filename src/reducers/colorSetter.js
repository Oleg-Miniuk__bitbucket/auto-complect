const defaultState = {
  id: 0,
  currentColor: "none",
  currentColorRus: "Выберите цвет",
};

export default function colorSetter(currentCar = defaultState, action) {
  switch (action.type) {
    case "color/set":
      return {
        id: action.id,
        currentColor: action.color,
        currentColorRus: action.colorRus,
        currentCarPic: action.source,
      };
    default:
      return currentCar;
  }
}
