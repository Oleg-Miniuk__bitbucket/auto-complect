const defaultState = {
  id: "",
  managerName: "",
  valueAddress: "",
  valueEmail: "",
  valuePhone: "",
};

export default function menagerContactDetailsReducer(
  stateMenagerContactDetails = defaultState,
  action
) {
  switch (action.type) {
    case "managerCurrent":
      return {
        id: action.id,
        managerName: action.managerName,
        valueAddress: action.valueAddress,
        valueEmail: action.valueEmail,
        valuePhone: action.valuePhone,
      };
    default:
      return stateMenagerContactDetails;
  }
}
