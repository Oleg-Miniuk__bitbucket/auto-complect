const defaultState = {
  totalPriceByn: 0,
  totalPriceRub: 0,
  basePriceByn: 0,
  basePriceRub: 0,
  additionalOptionsPriceByn: 0,
  additionalOptionsPriceRub: 0,
  reequipHullByn: 0,
  reequipHullRub: 0,
  reequipHullUsd: 0,
  reequipHullOptionsByn: 0,
  reequipHullOptionsRub: 0,
  reequipHullOptionsUsd: 0,
  overridenPriceByn: 0,
  overridenPriceRub: 0,
  overridenPriceUsd: 0,
};

export default function priceTracker(totalCost = defaultState, action) {
  switch (action.type) {
    case "priceTracker/init":
      return {
        ...totalCost,
        basePriceByn: action.priceByn,
        basePriceRub: action.priceRub,
      };
    case "additionalOptionsPrice/set":
      return {
        ...totalCost,
        additionalOptionsPriceByn: action.priceByn,
        additionalOptionsPriceRub: action.priceRub,
      };
    case "reequipHullPrice/set":
      return {
        ...totalCost,
        reequipHullByn: action.priceByn,
        reequipHullRub: action.priceRub,
        reequipHullUsd: action.priceUsd,
      };
    case "reequipHullOptionsPrice/set":
      return {
        ...totalCost,
        reequipHullOptionsByn: action.priceByn,
        reequipHullOptionsUsd: action.priceUsd,
      };
    case "overridenPrice/set":
      return {
        ...totalCost,
        overridenPriceByn: action.priceByn,
        overridenPriceRub: action.priceRub,
        overridenPriceUsd: action.priceUsd,
      };
    case "totalPrice/update":
      return {
        ...totalCost,
        totalPriceByn:
          totalCost.basePriceByn +
          totalCost.additionalOptionsPriceByn +
          totalCost.reequipHullByn +
          totalCost.reequipHullOptionsByn,
        totalPriceRub:
          totalCost.basePriceRub +
          totalCost.additionalOptionsPriceRub +
          totalCost.reequipHullRub +
          totalCost.reequipHullOptionsRub,
      };
    default:
      return totalCost;
  }
}
