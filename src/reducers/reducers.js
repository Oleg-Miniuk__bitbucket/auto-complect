import hullTypes from "./hull_types_reducer";
import colorSetter from "./colorSetter";
import toggleBtnLight from "./toggleButtonLight_reducer";
import priceTracker from "./priceTracker";
import additionalOptions from "./additionalOptionsReducer";
import toggleBtnBus from "./toggleButtonBus_reducer";
import toggleBtnCargo from "./toggleButtonCargo_reducer";
import retoolBtn from "./retoolBtn_reducer";
import rubExRateTracker from "./rubExRateTracker";
import usdExRateTracker from "./usdExRateTracker";
import deliveryTime from "./deliveryTime";
import menagerContactDetailsReducer from "./menegerContactDetailsReducer";
import reequipToggle from "./reequipToggle";
import reequipSOption from "./reequipOptionsReducer";

const reducers = {
  hullTypes,
  colorSetter,
  priceTracker,
  reequipSOption,
  additionalOptions,
  toggleBtnBus,
  toggleBtnCargo,
  toggleBtnLight,
  retoolBtn,
  rubExRateTracker,
  usdExRateTracker,
  deliveryTime,
  menagerContactDetailsReducer,
  reequipToggle,
};

export default reducers;
