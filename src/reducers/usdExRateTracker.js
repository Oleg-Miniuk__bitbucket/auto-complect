export default function usdExRateTracker(usdExRate = 0, action) {
  switch (action.type) {
    case "USDExRate/set":
      return {
        usdExRate: action.officialExRate,
      };
    default:
      return usdExRate;
  }
}
