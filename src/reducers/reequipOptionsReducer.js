export default function reequipSOption(selectPositions = [], action) {
  switch (action.type) {
    case "reequipSOption/set":
      return {
        selectPositions: action.selectPositions,
      };
    default:
      return selectPositions;
  }
}
