export default function deliveryTime(selectDate = [], action) {
  switch (action.type) {
    case "deliveryTime/set":
      return {
        year: action.year,
        month: action.month,
        monthLocalised: action.monthLocalised,
        rawDate: action.rawDate,
      };
    default:
      return selectDate;
  }
}
