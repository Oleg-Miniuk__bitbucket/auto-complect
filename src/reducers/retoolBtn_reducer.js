const defaultState = {
  nameCarRetoolBtn: "",
};

export default function retoolBtn(stateRetoolBtn = defaultState, action) {
  switch (action.type) {
    case "nameCar":
      return {
        nameCarRetoolBtn: action.nameCarRetoolBtn,
      };
    default:
      return stateRetoolBtn;
  }
}
