export default function additionalOptions(selectOptions = [], action) {
  switch (action.type) {
    case "additionalOptions/set":
      return {
        selectOptions: action.selectOptions,
      };
    default:
      return selectOptions;
  }
}
