export default function rubExRateTracker(rubExRate = 0, action) {
  switch (action.type) {
    case "RUBExRate/set":
      return {
        rubExRate: action.officialExRate * 10,
      };
    default:
      return rubExRate;
  }
}
