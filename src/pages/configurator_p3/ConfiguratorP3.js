import React from "react";
import AppHeader from "../../components/app-header/AppHeader";
import RequestExecutionTabs from "../../components/request-execution-tabs/RequestExecutionTabs";
import PooledСomponent from "../../components/managerContactDetails/BlockFordCardsAndManagerContactDetails";
import RequestExecution from "../../components/RequestExecution";
import CarDescriptionWithoutPrice from "../../components/car-description/CarDescriptionWithoutPrice";

const css = `
  .flex {
    display: flex;
    width: 1128px;
  }`;

export default function ConfiguratorP3() {
  return (
    <div>
      <style type="text/css">{css}</style>

      <AppHeader />
      <RequestExecution />
      <div className="App">
        <CarDescriptionWithoutPrice />
      </div>
      <PooledСomponent />

      <RequestExecutionTabs />
    </div>
  );
}
