import React from "react";
import AppHeader from "../../components/app-header/AppHeader";
import ReEquipment from "../../components/ReEquipment";
import RefitTabs from "../../components/refit-tabs/RefitTabs";
import ReEquipmentCardImg from "../../components/re-equipment-text/ReEquipmentCardImg";
import Text from "../../components/re-equipment-text/ReEquipmentText";

export default function ConfiguratorP2() {
  return (
    <div>
      <AppHeader />
      <ReEquipment />
      <Text />
      <ReEquipmentCardImg />
      <RefitTabs />
    </div>
  );
}
