/* eslint-disable no-plusplus */
import React from "react";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
  Font,
  Image,
} from "@react-pdf/renderer";
import { useHistory } from "react-router-dom";
import Html from "react-pdf-html";

Font.register({
  family: "Roboto",
  src: "https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-medium-webfont.ttf",
});

export default function PdfPage() {
  const history = useHistory();
  console.log(history?.location.state.name);

  const rowsbasicOptions = history?.location.state.basicOptions.concat(
    history?.location.state.reequipBasicOptions
  );
  const rowbasicOptions = rowsbasicOptions
    ? rowsbasicOptions.map((baseOption) => <li>{baseOption}</li>)
    : [];

  const rowbaseOption = rowbasicOptions.map((item) => {
    return `
-${item.props.children}`;
  });

  const htmlbaseOption = `<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <style>
  .pre {
  font-size: 11px;
  }
  </style>
   <pre class = "pre">${rowbaseOption.join("")}</pre>
  </html>
  `;

  const rowsoptionsName = history?.location.state.optionsName
    ? history?.location.state.optionsName.map((optionName) => (
        <li>{optionName.option_name}</li>
      ))
    : [];

  const rowoptionsName = rowsoptionsName.map((item) => {
    return `
-${item.props.children}`;
  });

  const htmloptionsName = `<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <style>
  .pre {
  font-size: 11px;
  }
  </style>
   <pre class = "pre">${rowoptionsName.join("")}</pre>
  </html>
  `;

  const nameCar = history?.location.state.nameCar;
  let fullNameCar;

  if (nameCar === "light") {
    fullNameCar = "Легковой";
  }
  if (nameCar === "bus") {
    fullNameCar = "Автобус";
  }
  if (nameCar === "cargo") {
    fullNameCar = "Грузопассажирский";
  }

  let overridenPrice = history?.location.state.overridenPriceByn;
  if (history?.location.state.overridenPriceByn === 0) {
    overridenPrice = history?.location.state.totalPriceByn;
  }

  const selectedOptions = history?.location.state.selectedOptionsRows
    ? history?.location.state.selectedOptionsRows.map((selectedOptionsRow) => (
        <li>{selectedOptionsRow.content}</li>
      ))
    : [];

  const selectOptions = selectedOptions.map((item) => {
    return `
-${item.props.children}`;
  });

  const htmlselectedOptions = `<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <style>
  .pre {
  font-size: 11px;
  }
  </style>
   <pre class = "pre">${selectOptions.join("")}</pre>
  </html>
  `;

  const styles = StyleSheet.create({
    document: {
      width: "100%",
      height: "100%",
      textAlign: "left",
      marginBottom: 20,
    },
    pdfviewer: {
      width: "1250px",
      height: "1200px",
    },
    page: {
      marginBottom: 20,
      paddingVertical: 20,
      backgroundColor: "#FFFFFF",
    },
    container1: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    container2: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      height: 170,
      marginTop: 7,
    },
    imagelogo: {
      width: "30%",
      padding: 30,
      paddingLeft: 50,
    },
    imagecar: {
      width: "50%",
      padding: 0,
      paddingLeft: 5,
    },
    text: {
      display: "flex",
      fontSize: "9px",
      width: "70%",
      fontFamily: "Roboto",
      textAlign: "left",
      justifyContent: "flex-end",
      paddingHorizontal: 55,
      paddingVertical: 40,
    },
    text1: {
      display: "flex",
      fontSize: "11px",
      textIndent: 20,
      width: "100%",
      padding: 0,
      margin: 0,
      fontFamily: "Roboto",
      textAlign: "left",
      justifyContent: "flex-start",
      paddingLeft: 20,
      paddingRight: 20,
    },
    html: {
      fontSize: "11px",
      width: "100%",
      fontFamily: "Roboto",
      textAlign: "left",
      padding: 0,
      margin: 0,
      marginTop: 2,
      paddingLeft: 20,
      paddingRight: 20,
    },
    text2: {
      display: "flex",
      fontSize: "11px",
      textIndent: 20,
      width: "100%",
      fontFamily: "Roboto",
      textAlign: "left",
      justifyContent: "flex-start",
      paddingLeft: 0,
      marginVertical: 5,
      paddingRight: 20,
    },

    detailsconteiner: {
      maxheight: "auto",
      width: "50%",
      backgroundColor: "#FFFFFF",
    },
    namecar: {
      fontFamily: "Roboto",
      backgroundColor: "#2d96cd",
      fontSize: "12px",
      fontWeight: "bold",
      pagging: 2,
      color: "#FFFFFF",
      marginRight: 20,
    },
    head: {
      fontFamily: "Roboto",
      backgroundColor: "#2d96cd",
      fontSize: "12px",
      fontWeight: "bold",
      pagging: 2,
      color: "#FFFFFF",
      marginVertical: 5,
      marginHorizontal: 16,
    },
    varietyofdetails: {
      fontFamily: "Roboto",
      textAlign: "left",
      width: "100%",
      backgroundColor: "#FFFFFF",
      fontSize: "11px",
      marginTop: 2,
      padding: 0,
    },
    imageauto: {
      marginHorizontal: 16,
      width: 400,
      height: 174,
    },
  });

  return (
    <PDFViewer style={styles.pdfviewer}>
      <Document style={styles.document}>
        <Page size="A4" style={styles.page}>
          <View style={styles.container1}>
            <Image style={styles.imagelogo} src="/logo-atlant.png" />
            <Text style={styles.text}>
              ООО «Автоцентр «Атлант-М Боровая» 223053 Минский р-н, д. Боровая,
              2 тел.: + 375 (29) 766-44-44, borovaya.by
            </Text>
          </View>
          <Text style={styles.text1}>
            ООО Автоцентр «Атлант-М Боровая» - официальный дилер Ford в
            Республике Беларусь, предлагает Вам рассмотреть приобретение нового
            автомобиля Ford на следующих условиях:
          </Text>
          {/* начало блока с автомобилем и деталями */}
          <View style={styles.container2}>
            <Image
              style={styles.imagecar}
              src={history?.location.state.source}
            />
            <View style={styles.detailsconteiner}>
              <Text style={styles.namecar}>
                {history?.location.state.name} Цельнометаллический фургон
              </Text>

              <Text style={styles.varietyofdetails}>
                Двигатель: {history?.location.state.engine}
              </Text>
              <Text style={styles.varietyofdetails}>
                Тип топлива: {history?.location.state.fueltype}
              </Text>
              <Text style={styles.varietyofdetails}>
                Трансмиссия : {history?.location.state.transmission}
              </Text>
              <Text style={styles.varietyofdetails}>
                Цвет: {history?.location.state.colour}
              </Text>
              <Text style={styles.varietyofdetails}>
                Cалон: {history?.location.state.salon}
              </Text>
              <Text style={styles.varietyofdetails}>
                Тип привода: {history?.location.state.typeofdrive}
              </Text>
              <Text style={styles.varietyofdetails}>
                Мощность двигателя: {history?.location.state.enginepower}
              </Text>
              <Text style={styles.varietyofdetails}>
                Коробка передач: {history?.location.state.gearbox}
              </Text>
            </View>
          </View>
          {/* конец блока с автомобилем и деталями */}
          <Text style={styles.head}>
            Стоимость по прайс-листу: {history?.location.state.totalPriceByn} {}
            BYN
          </Text>
          <Text style={styles.head}>
            Специальная цена: {overridenPrice} {}
            BYN
          </Text>
          <Text style={styles.head}>
            Срок поставки: {history?.location.state.deliveryTimeMonth}{" "}
            {history?.location.state.deliveryTimeYear}
          </Text>
          <Text style={styles.head}>Базовая комплектация:</Text>
          <Text style={styles.text1}>
            <Html style={styles.html}>{htmlbaseOption}</Html>
          </Text>
          <Text style={styles.head}>Опциональное оборудование:</Text>
          <Text style={styles.text1}>
            <Html style={styles.html}>{htmloptionsName}</Html>
          </Text>
          {/* начало блока Тип транспортного срества */}
          <View wrap={false}>
            <Text style={styles.head}>Тип транспортного срества:</Text>
            <Text style={styles.text2}>
              {`${fullNameCar} ${history?.location.state.selectedSitsNum} на базе ${history?.location.state.name}`}
            </Text>
            <Image
              style={styles.imageauto}
              src={history?.location.state.reequipToggle}
            />
          </View>
          {/* конец блока Тип транспортного срества */}
          <Text style={styles.head}>Опции переобородувания:</Text>
          <Text style={styles.text1}>
            <Html style={styles.html}>{htmlselectedOptions}</Html>
          </Text>
          <View wrap={false}>
            <Text style={styles.head}>Гарантия на автомобиль:</Text>
            <Text style={styles.text2}>2 года без ограничения пробега</Text>
            <Text style={styles.text2}>12 лет от сквозной коррозии кузова</Text>
            <Text style={styles.text2}> 2 года на лакокрасочное покрытие</Text>
            <Text style={styles.text2}>
              Страна происхождения товара – Российская Федерация.
            </Text>
          </View>
          <View wrap={false}>
            <Text style={styles.head}>Межсервисный интервал:</Text>
            <Text style={styles.text2}>
              Один раз в год ИЛИ через 20 000 км пробега автомобиля
            </Text>
          </View>
          <View wrap={false}>
            <Text style={styles.head}>Контактное лицо:</Text>
            <Text style={styles.text2}>
              Cпециалист отдела корпаративных продаж:
            </Text>
            <Text style={styles.text2}>
              {history?.location.state.managerName}
            </Text>
            <Text style={styles.text2}>
              {history?.location.state.valuePhone}
            </Text>
            <Text style={styles.text2}>
              {history?.location.state.valueEmail}
            </Text>
          </View>
        </Page>
      </Document>
    </PDFViewer>
  );
}
