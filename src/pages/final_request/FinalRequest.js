/* eslint-disable class-methods-use-this */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import html2canvas from "html2canvas";
import JsPDF from "jspdf";
import { Page, Text, StyleSheet, Image } from "@react-pdf/renderer";
import Container from "@mui/material/Container";
import { renderToString } from "react-dom/server";
import FinalVehicleInformation from "../../components/finalVehicleInformation/FinalVehicleInformation";

// import NextBtn from "../../components/generic/NextBtn";
import store from "../../store/store";

const mapStateToProps = (state) => {
  return {
    name: state.hullTypes.name,
  };
};

/* start function that converts the react component to HTML */

function RenderToStringFinalRequest() {
  return renderToString(<FinalRequest />);
}

/* start save button */

export class SaveButton extends Component {
  constructor(props) {
    super(props);
    this.state = { canvas: null };
  }

  printDocument() {
    const input = document.getElementById("divToPrint");
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new JsPDF();
      pdf.addImage(imgData, "JPEG", 0, 0);
      pdf.save("Комплектация автомобиля Ford.pdf");
    });
  }

  render() {
    return (
      <div className="nextBtn">
        <button type="button" onClick={this.printDocument}>
          Сохранить
        </button>
      </div>
    );
  }
}

/* end save button */

function FinalRequest() {
  const styles = StyleSheet.create({
    app: {
      textAlign: "center",
      width: 1128,
      margin: "auto",
      padding: "auto",
    },
    document: {
      width: "100%",
      height: "100%",
    },
    page: {
      backgroundColor: "#FFFFFF",
    },
    container: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "flex-start",
      padding: 0,
      alignItems: "center",
    },
    image: {
      width: "12%",
      padding: 30,
      paddingLeft: 0,
    },
    text: {
      display: "flex",
      flexDirection: "column",
      fontSize: "13px",
      width: "30%",
      fontFamily: "Roboto",
      textAlign: "left",
      justifyContent: "center",
      alignItems: "center",
      marginLeft: 700,
    },
    p: {
      margin: "1px",
    },
    text1: {
      display: "flex",
      fontSize: "15px",
      textIndent: 20,
      width: "100%",
      fontFamily: "Roboto",
      textAlign: "left",
      justifyContent: "flex-start",
      paddingBottom: 12,
    },
    pdfviewer: {
      width: "1250px",
      height: "1200px",
    },
  });

  return (
    <div style={styles.app}>
      <Page size="A4" style={styles.page}>
        <Container style={styles.container} fixed>
          <img src="/logo-atlant.png" style={styles.image} alt="" />
          <Text style={styles.text}>
            <p style={styles.p}>
              <b>ООО «Автоцентр «Атлант-М Боровая»</b>
            </p>
            <p style={styles.p}>
              223053 Минский р-н, д. Боровая, 2
              <p style={styles.p}>тел.: + 375 (29) 766-44-44, borovaya.by</p>
            </p>
          </Text>
        </Container>

        <Text style={styles.text1}>
          ООО Автоцентр «Атлант-М Боровая» - официальный дилер Ford в Республике
          Беларусь, предлагает Вам рассмотреть приобретение нового автомобиля
          Ford на следующих условиях:
        </Text>
        <FinalVehicleInformation />
      </Page>
    </div>
  );
}

export default connect(mapStateToProps)(RenderToStringFinalRequest);
