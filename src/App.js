import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import StartingPage from "./pages/starting_page/StartingPage";
import ConfiguratorP1 from "./pages/configurator_p1/ConfiguratorP1";
import ConfiguratorP2 from "./pages/configurator_p2/ConfiguratorP2";
import ConfiguratorP3 from "./pages/configurator_p3/ConfiguratorP3";
import PdfPage from "./pages/pdf_page/PdfPage";
import FinalRequest from "./pages/final_request/FinalRequest";
import getRubExRate from "./functions/getRubExRate";
import getUsdExRate from "./functions/getUsdExRate";
import store from "./store/store";
import "./App.css";

function App() {
  getRubExRate();
  getUsdExRate();
  setInterval(getRubExRate, 3600000);
  setInterval(getUsdExRate, 3600000);
  return (
    <Provider store={store}>
      <Router>
        <Route path="/" exact>
          <StartingPage />
        </Route>
        <Route path="/configurator_p1" component={ConfiguratorP1} />
        <Route path="/configurator_p2" component={ConfiguratorP2} />
        <Route path="/сonfigurator_p3" component={ConfiguratorP3} />
        <Route path="/making_a_request" component={FinalRequest} />
        <Route path="/pdfPage" component={PdfPage} />
      </Router>
    </Provider>
  );
}
export default App;
