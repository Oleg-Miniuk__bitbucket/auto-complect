import { dispatch } from "../store/store";

async function getUsdExRate() {
  const date = new Date();
  const year = date.getFullYear().toString();
  const month = (date.getMonth() + 1).toString();
  const day = date.getDate().toString();
  const urlFinal = `https://www.nbrb.by/api/exrates/rates/840?ondate=${year}-${month}-${day}&parammode=1`;
  await fetch(urlFinal)
    .then((resp) => resp.json())
    .then(function fetch(data) {
      const officialExRate = data.Cur_OfficialRate;
      dispatch({
        type: "USDExRate/set",
        officialExRate: data.Cur_OfficialRate,
      });
      return officialExRate;
    })
    .catch(function fetch() {
      console.error('"error"');
    });
}
export default getUsdExRate;
